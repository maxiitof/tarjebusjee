/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.XMLReader;

import java.io.IOException;
import java.io.StringReader;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author Mxio
 */
class LectorXML extends DefaultHandler {
 
    private final XMLReader xr;
 
    public LectorXML() throws SAXException {
        xr = XMLReaderFactory.createXMLReader();
        xr.setContentHandler(this);
        xr.setErrorHandler(this);
    }
 
    public void leer(final String archivoXML)
             throws IOException, SAXException {
        xr.parse(new InputSource(new StringReader(archivoXML)));
    }
 
    @Override
    public void startDocument() {
        //System.out.println("Comienzo del Documento XML");
    }
 
    @Override
    public void endDocument() {
        //System.out.println("Final del Documento XML");
    }
    
    @Override
    public void startElement(String uri, String name,
                             String qName, Attributes atts) {
        System.out.println("tElemento: " + name );
        for (int i = 0; i < atts.getLength(); i++) {
            System.out.println("ttAtributo: " + atts.getLocalName(i) + " = " + atts.getValue(i));
        }
    }
 
    @Override
    public void endElement(String uri, String name,
                                 String qName) {
        System.out.println("tFin Elemento: " + name);
    }
}