/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities.clasesabstractas;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;

public abstract class Persona {
    // ----- Definición de Variables Miembro -----      
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaNacimiento;
    @Column
    private String domicilioFiscal;
    
    // ----- Definición de Operaciones -----
    // ----- Getters y Setter -----
    /**
     * @return the fechaNacimiento
     */
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }
  
    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(Date unaFecha) {
        this.fechaNacimiento = unaFecha;
    }
  
    /**
     * @return the domicilioFiscal
     */
    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }
  
    /**
     * @param domicilioFiscal the domicilioFiscal to set
     */
    public void setDomicilioFiscal(String unDomicilioFiscal) {
        this.domicilioFiscal = unDomicilioFiscal;
    }     
} 