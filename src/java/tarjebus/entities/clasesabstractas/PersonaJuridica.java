/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities.clasesabstractas;
  
import javax.persistence.Column;

/**
 *
 * @author Fer500
 */
public abstract class PersonaJuridica {
    // ----- Definición de Variables Miembro -----
    @Column
    private String cuit;
    @Column
    private String razonSocial;
    @Column
    private String ivaInscripto;
    
    // ----- Definición de Operaciones -----
    // ----- Getters y Setter -----
        /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }
  
    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String unaRazonSocial) {
        this.razonSocial = unaRazonSocial;
    }
  
    /**
     * @return the ivaInscripto
     */
    public String getIvaInscripto() {
        return ivaInscripto;
    }
  
    /**
     * @param ivaInscripto the ivaInscripto to set
     */
    public void setIvaInscripto(String unTipoIva) {
        this.ivaInscripto = unTipoIva;
    }
    
    /**
     * @return the cuit
     */
    public String getCuit() {
        return cuit;
    }
  
    /**
     * @param cuit the cuit to set
     */
    public void setCuit(String unCuit) {
        this.cuit = unCuit;
    }
      
      
} 