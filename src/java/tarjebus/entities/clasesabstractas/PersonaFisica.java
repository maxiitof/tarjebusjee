/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities.clasesabstractas;

import javax.persistence.Column;

/**
 *
 * @author Fer500
 */
public abstract class PersonaFisica extends Persona {
    // ----- Definición de Variables Miembro -----
    @Column
    private int dni;
    @Column
    private String nombre;
    @Column
    private String apellido;

    // ----- Definición de Operaciones -----
    // ----- Getters y Setter -----
    /**
     * @return the DNI
     */
    public int getDNI() {
        return dni;
    }
  
    /**
     * @param dni the DNI to set
     */
    public void setDNI(int unDNI) {
        this.dni = unDNI;
    }
  
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }
  
    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }    

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String unApellido) {
        this.apellido = unApellido;
    }
} 