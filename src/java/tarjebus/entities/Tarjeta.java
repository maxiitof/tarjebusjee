/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities;
  
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Column;
import javax.persistence.OneToOne;
/**
 *
 * @author Fer500
 */
@Entity
public class Tarjeta implements Serializable {
    // ----- Definición de Variables Miembro -----
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String estado;
    @Column
    private String tipoUsuario;
    @Column
    @Temporal(TemporalType.DATE)
    private Date fechaExpedido;
    @Column
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    
    @OneToOne
    private Pasajero pasajero;
    
    // ----- Definición de Operaciones -----   
    public Long getId() {
        return id;
    }
  
    public void setId(Long id) {
        this.id = id;
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
  
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarjeta)) {
            return false;
        }
        Tarjeta other = (Tarjeta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
  
    @Override
    public String toString() {
        return "Otros.Tarjeta[ id=" + id + " ]";
    }
 
    // ----- Getters y Setter -----
    /**
     * @return the fechaExpedido
     */
    public Date getFechaExpedido() {
        return fechaExpedido;
    }
  
    /**
     * @param fechaExpedido the fecha_expedido to set
     */
    public void setFechaExpedido(Date unaFecha) {
        this.fechaExpedido = unaFecha;
    }
  
    /**
     * @return the fechaVencimiento
     */
    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }
  
    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(Date unaFecha) {
        this.fechaVencimiento = unaFecha;
    }
  
    /**
     * @return the tipoUsuario
     */
    public String getTipoUsuario() {
        return tipoUsuario;
    }
  
    /**
     * @param tipoUsuario the tipoUsuario to set
     */
    public void setTipo_usuario(String unTipoUsuario) {
        this.tipoUsuario = unTipoUsuario;
    }
  
    /**
     * @return the pasajero
     */
    public Pasajero getPasajero() {
        return pasajero;
    }
  
    /**
     * @param pasajero the pasajero to set
     */
    public void setPasajero(Pasajero pasajero) {
        this.pasajero = pasajero;
    }
  
    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }
  
    /**
     * @param estado the estado to set
     */
    public void setEstado(String unEstado) {
        this.estado = unEstado;
    }
      
} 