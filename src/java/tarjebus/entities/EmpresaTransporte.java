/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities;

import java.util.List;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import tarjebus.entities.clasesabstractas.PersonaJuridica;
import tarjebus.interfaces.GestionUsuarios;

/**
 *
 * @author Fer500
 */
@Entity
public class EmpresaTransporte extends PersonaJuridica 
                                implements Serializable, GestionUsuarios {
    // ----- Definición de Variables Miembro -----      
    @Column
    private String usuario;
    @Column
    private String clave;
    
    // NO CREO QUE ESTE ATRIBUTO VAYA, LA RELACION ESTA PRESENTE EN COLECTIVO
    // ELIMINAR SUS SETTERS Y GETTERS EN CASO DE HACERLO
    @OneToMany(mappedBy="empresaTransporte")
    private List<Colectivo> colectivos;
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // ----- Definición de Operaciones -----
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Usuarios.EmpresaTransporte[ id=" + id + " ]";
    }

    // ----- Getters y Setter -----
    /**
     * @return the usuario
     */
    @Override
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    @Override
    public void setUsuario(String unUsuario) {
        this.usuario = unUsuario;
    }

    /**
     * @return the contraseña
     */

    @Override
    public boolean compararClaves(String unaClave) {
    	// TODO Auto-generated method stub
	return (this.clave == null ? unaClave == null : this.clave.equals(unaClave));
    }
    /**
    * @param clave the clave to set
    */
    
    @Override
    public void setClave(String unaClave) {
	// TODO Auto-generated method stub
	this.clave = unaClave;
    }
    
    /**
     * @return the colectivos
     */
    public List<Colectivo> getColectivos() {
        return colectivos;
    }

    /**
     * @param colectivos the colectivos to set
     */
    public void setColectivos(List<Colectivo> colectivos) {
        this.colectivos = colectivos;
    }   
}