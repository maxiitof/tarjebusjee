/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities;
import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import tarjebus.entities.clasesabstractas.PersonaFisica;
  
/**
 *
 * @author Fer500
 */
@Entity
public class Pasajero extends PersonaFisica implements Serializable {
    // ----- Definición de Variables Miembro -----
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
  
    @Column
    private float saldo;
    
    @OneToMany(mappedBy="pasajero") //NO SE SI ESTO VA.
    private List<Tarjeta> tarjetas = new ArrayList<Tarjeta>();
    
    // ----- Definición de Operaciones -----
    public Long getId() {
        return id;
    }
  
    public void setId(Long id) {
        this.id = id;
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
  
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pasajero)) {
            return false;
        }
        Pasajero other = (Pasajero) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
  
    @Override
    public String toString() {
        return "Modelo.Pasajero[ id=" + id + " ]";
    }
  
    // ----- Getters y Setter -----
    /**
     * @return the saldo
     */
    public float getSaldo() {
        return saldo;
    }
  
    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
  
    /**
     * @return the tarjetas
     */
    public List<Tarjeta> getTarjetas() {
        return tarjetas;
    }
      
    /**
     * @param tarjetas the tarjetas to set
     */
    public void addTarjeta(Tarjeta unaTarjeta) {
        this.tarjetas.add(unaTarjeta);
    }
  
    
      
} 