/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities;
  
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
  
/**
 *
 * @author Fer500
 */
  
@Entity
public class Boleto implements Serializable {
    // ----- Definición de Variables Miembro -----
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaHoraEmision;
    
    @Column
    @NotNull
    private float monto;
    
    @Column
    @NotNull
    @OneToOne
    private Colectivo colectivo;
    
    @Column
    @NotNull
    @OneToOne
    private Tarjeta tarjeta;
    
      
    // ----- Definición de Operaciones -----
    public Long getId() {
        return id;
    }
  
    public void setId(Long id) {
        this.id = id;
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
  
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boleto)) {
            return false;
        }
        Boleto other = (Boleto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
  
    @Override
    public String toString() {
        return "Transacciones.Boleto[ id=" + id + " ]";
    }  
  
    // ----- Getters y Setter -----
    /**
     * @return the fechaHoraEmision
     */
    public Date getFechaHoraEmision() {
        return fechaHoraEmision;
    }

    /**
     * @param fechaHoraEmision the fechaHoraEmision to set
     */
    public void setFechaHoraEmision(Date fechaHoraEmision) {
        this.fechaHoraEmision = fechaHoraEmision;
    }

    /**
     * @return the monto
     */
    public float getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(float monto) {
        this.monto = monto;
    }
}
