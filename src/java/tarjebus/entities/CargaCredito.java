/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Mxio
 */
@Entity
public class CargaCredito implements Serializable {
    
    // ----- Definición de Variables Miembro -----
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaHoraTransaccion;
    @Column
    private double monto;
    @OneToOne
    private Tarjeta tarjeta;
   
    // ----- Constructor/es ------
    public CargaCredito(){
    }
    
    public CargaCredito (double unMonto){
	this.fechaHoraTransaccion = this.fechaHoraActual();
	this.monto = unMonto;
    }
    
    // ----- Definición de Operaciones -----.
    // ----- Definición de Operaciones -----
    public Long getId() {
        return id;
    }
  
    public void setId(Long id) {
        this.id = id;
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
  
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CargaCredito)) {
            return false;
        }
        CargaCredito other = (CargaCredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
  
    @Override
    public String toString() {
        return "Modelo.Pasajero[ id=" + id + " ]";
    }
  
    // ----- Getters y Setter -----
    public Date getFechaHoraTransaccion() {
        return fechaHoraTransaccion;
    }
	
    private Date fechaHoraActual(){
	Calendar Calendario = new GregorianCalendar();
	int Dia = Calendario.get(Calendar.DATE);
	int Mes = Calendario.get(Calendar.MONTH);
	int Anio = Calendario.get(Calendar.YEAR)-1900;
	int Hora = Calendario.get(Calendar.HOUR); 
	int Minuto = Calendario.get(Calendar.MINUTE);	
	int Segundo = Calendario.get(Calendar.SECOND);
	Date fechaHoraActual = new Date(Anio, Mes, Dia, Hora, Minuto, Segundo);
	return fechaHoraActual;
    }
    public Tarjeta obtenerTarjeta(){
        return this.tarjeta;
    }
}
