/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities;
  
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToOne;
  
/**
 *
 * @author Fer500
 */
@Entity
public class Colectivo implements Serializable {
    // ----- Definición de Variables Miembro -----
    @Column
    private String patente;
    @Column
    private String linea;
    @Column
    private String interno;
    
    //NO CREO QUE VAYA ESTE ATRIBUTO.
    @OneToOne// Preguntar si hace falta agregar (mappedby="id");
    private EmpresaTransporte empresaTransporte;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
  
    // ----- Definición de Operaciones -----.
    public Long getId() {
        return id;
    }
  
    public void setId(Long id) {
        this.id = id;
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
  
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Colectivo)) {
            return false;
        }
        Colectivo other = (Colectivo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
  
    @Override
    public String toString() {
        return "Modelo.Colectivo[ id=" + id + " ]";
    }

    // ----- Getters y Setter -----
      
    /**
     * @return the patente
     */
        public String getPatente() {
        return this.patente;
    }
  
    /**
     * @param patente the patente to set
     */
    public void setPatente(String unaPatente) {
        this.patente = unaPatente;
    }
  
    /**
     * @return the linea
     */
    public String getLinea() {
        return this.linea;
    }
  
    /**
     * @param linea the linea to set
     */
    public void setLinea(String unaLinea) {
        this.linea = unaLinea;
    }
  
    /**
     * @return the interno
     */
    public String getInterno() {
        return interno;
    }
  
    /**
     * @param interno the interno to set
     */
    public void setInterno(String unInterno) {
        this.interno = unInterno;
    }
  
    /**
     * @return the empresaTransporte
     */
    public EmpresaTransporte getEmpresaTransporte() {
        return empresaTransporte;
    }
  
    /**
     * @param empresaTransporte the empresaTransporte to set
     */
    public void setEmpresaTransporte(EmpresaTransporte unaEmpresaTransporte) {
        this.empresaTransporte = unaEmpresaTransporte;
    }
      
} 