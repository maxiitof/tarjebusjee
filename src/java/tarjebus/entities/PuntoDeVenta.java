/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import tarjebus.entities.clasesabstractas.PersonaJuridica;
import tarjebus.interfaces.GestionUsuarios;

/**
 *
 * @author Mxio
 */
@Entity
public class PuntoDeVenta extends PersonaJuridica implements Serializable, GestionUsuarios {
    // ----- Definición de Variables Miembro -----
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column    
    private String usuario;
    @Column
    private String clave;
    
    // ----- Definición de Operaciones -----
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PuntoDeVenta)) {
            return false;
        }
        PuntoDeVenta other = (PuntoDeVenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tarjebus.entities.PuntoDeVenta[ id=" + id + " ]";
    }
    
    @Override
    public void setUsuario(String unUsuario) {
	// TODO Auto-generated method stub
	this.usuario = unUsuario;
    }
    @Override
    public String getUsuario() {
	// TODO Auto-generated method stub
	return this.usuario;
    }

    @Override
    public void setClave(String unaClave) {
	// TODO Auto-generated method stub
	this.clave = unaClave;
    }
    
    @Override
    public boolean compararClaves(String unaClave) {
    	// TODO Auto-generated method stub
	return (this.clave == null ? unaClave == null : this.clave.equals(unaClave));
    }
    // ----- Getters y Setter -----
}
