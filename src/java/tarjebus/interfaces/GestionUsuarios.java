/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.interfaces;

/**
 *
 * @author Mxio
 */
public interface GestionUsuarios {
    public void setUsuario(String unUsuario);
    public String getUsuario();
    public void setClave(String unaClave);
    public boolean compararClaves(String unaClave); 
}
