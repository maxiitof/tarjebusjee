/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.webservice;

import java.io.StringReader;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.InputSource;

/**
 * REST Web Service
 *
 * @author Mxio
 */

@Path("/transaccion/boletos")
@Produces("application/xml")
public class TransaccionBoletosResource {

    @Context
    private UriInfo context;
    /**
     * Creates a new instance of TransaccionBoletosResource
     */
    public TransaccionBoletosResource() {
    }
    
    /**
     * Retrieves representation of an instance of
     * tarjebus.webservice.TransaccionBoletosResource
     *
     * @return an instance of java.lang.String
     */
    
    @GET
    @Path("/{empresa}/{monto}/{usuario}")
    @Produces("text/plain)")
    public String procesarBoletoAsPlainText(@PathParam("empresa") String unaEmpresa,
                                            @PathParam("monto") double unMonto,
                                            @PathParam("usuario") int unUsuario) {
        // Verificar que exista la empresa y el colectivo;
        // Verificar que la tarjeta exista y su estado sea activo;
        // Identificar el pasajero asociada a la tarjeta y verificar su saldo;
        // Registrar la transacción;
        // Confeccionar respuesta application/xml o text/plain;
        // Retornar respuesta;
        String nombre;
        double saldo;
        String exito;
        String razon;

        switch (unUsuario) {
            case 1:
                saldo = 2.33;
                nombre = "Julio";
                break;
            case 2:
                saldo = 10.00;
                nombre = "Jorge";
                break;
            case 3:
                saldo = 6.00;
                nombre = "Andres";
                break;
            default:
                saldo = 0.00;
                nombre = "Desconocido";
        }

        if (unMonto > saldo) {
            exito = "No";
            razon = "El usuario " + nombre + " no posee credito suficiente. Su saldo actual es: " + saldo;
        } else {
            exito = "Si";
            saldo -= unMonto;
            razon = "Su saldo restantes es $" + Double.toString(saldo);
        }
        String newXML;
        newXML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
                + "<boleto>\n"
                + "<usuario>" + nombre + "</usuario>\n"
                + "<exito>" + exito + "</exito>\n"
                + "<razon>" + razon + "</razon>\n"
                + "</boleto>";
        return newXML;
    }

    /**
     * PUT method for updating or creating an instance of TransaccionBoletosResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    
    @PUT
    @Produces("application/xml")
    @Consumes("application/xml")
    public String procesarBoletoAsXML(String content) throws XPathExpressionException {

        double saldo, unMonto;
        String nombre, razon, exito, usuario, monto;
        int unUsuario;
        StringReader xml;
        InputSource inputSource;
        
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();
        
        xml = new StringReader(content);
        inputSource = new InputSource(xml);
        usuario = (String) xpath.evaluate("/boleto/usuario", inputSource, XPathConstants.STRING);
        unUsuario = Integer.parseInt(usuario);
        
        xml = new StringReader(content);
        inputSource = new InputSource(xml);
        monto = (String) xpath.evaluate("/boleto/monto", inputSource, XPathConstants.STRING);
        unMonto = Double.parseDouble(monto);

        switch (unUsuario) {
            case 1:
                saldo = 2.33;
                nombre = "Julio";
                break;
            case 2:
                saldo = 10.00;
                nombre = "Jorge";
                break;
            case 3:
                saldo = 6.00;
                nombre = "Andres";
                break;
            default:
                saldo = 0.00;
                nombre = "Desconocido";
        }

        if (unMonto > saldo) {
            exito = "No";
            razon = "El usuario " + nombre + " no posee credito suficiente. Su saldo actual es: " + saldo;
        } else {
            exito = "Si";
            saldo -= unMonto;
            razon = "Su saldo restantes es $" + Double.toString(saldo);
        }
    
        String newXML;
        newXML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
                + "<boleto>\n"
                + "<usuario>" + nombre + "</usuario>\n"
                + "<exito>" + exito + "</exito>\n"
                + "<razon>" + razon + "</razon>\n"
                + "</boleto>";
        
        return newXML;
    }
}
