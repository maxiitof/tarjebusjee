/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjebus.webservice;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author Mxio
 */
@Path("/consultas/usuario/{dni}")
public class CreditoPasajeroResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CreditoPasajeroResource
     */
    public CreditoPasajeroResource() {
    }

    /**
     * Retrieves representation of an instance of
     * tarjebus.webservice.CreditoPasajeroResource
     *
     * @param dni resource URI parameter
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/plain")
    public String getText(@PathParam("dni") int dni) {
        //TODO return proper representation object
            String saldo;
            saldo = "Su saldo es: $";
            switch (dni){
                case 333: saldo += 5.00; break;
                case 444: saldo += 7.00; break;
                case 555: saldo += 0.00; break;
                default: saldo = "Usuario inexistente.";
            }
            return saldo;
    }

}
